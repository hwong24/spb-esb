package org.spb;

import java.util.UUID;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

public class GenerateUUID extends AbstractMessageTransformer {	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		//"correlationID":"123456",
		UUID uuid = UUID.randomUUID(); 
		String randomUUIDString = uuid.toString();
	    message.setInvocationProperty("correlationID", randomUUIDString);
		return message.getPayload();
	}
}
