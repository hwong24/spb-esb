CREATE VIEW main.srv_list_trx AS
select 
--transformacion asignada
	RTRIM(s.srv_apl_id_origen) AS srv_apl_id_origen, 
	RTRIM(s.srv_apl_id_destino) as srv_apl_id_destino,
	RTRIM(s.srv_operacion_origen) AS srv_operacion_origen,
	RTRIM(s.srv_apl_operacion_destino) as srv_operacion_destino,
	RTRIM(s.srv_operacion_servicio) AS srv_operacion_servicio,
	RTRIM(t.tra_id) AS srv_transformacion,
-- proveedor
	RTRIM(t.tra_nombre_proveedor) AS srv_nombre_proveedor,
	RTRIM(s.srv_formato) AS srv_formato,
	RTRIM(s.srv_path_carga) AS srv_path_carga,
	RTRIM(s.srv_metodo_http) AS srv_metodo_http,
	RTRIM(a.apl_host) AS srv_host,
	RTRIM(a.apl_port) AS srv_port,
	RTRIM(a.apl_path_service) AS srv_path_service_request,
	RTRIM(a.apl_host) + ':'+ RTRIM(a.apl_port) + RTRIM(a.apl_path_service) AS srv_endpoint,
	RTRIM(s.srv_apl_name_service_soap) AS srv_apl_name_service_soa,
	RTRIM(s.srv_apl_port_service_soap) AS srv_apl_port_service_soa,
	RTRIM(s.srv_param_type) AS srv_param_type,
	RTRIM(s.srv_tipo_protocolo_http) AS srv_tipo_protocolo_http,
	RTRIM(s.srv_tipo_respuesta) AS srv_tipo_respuesta,
	RTRIM(s.srv_headers_request) AS srv_headers_request
from spb_service as s
inner join spb_application as a on a.apl_id = s.srv_apl_id_origen
inner join spb_transformation as t on t.tra_operacion_origen = s.srv_operacion_origen
where t.tra_apl_id = s.srv_apl_id_origen;

