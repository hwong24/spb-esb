drop table main.spb_application;
drop table main.spb_service;
drop table main.spb_transformation;

CREATE TABLE main.spb_application (
	apl_id text NOT NULL,
	apl_nombre text NULL,
	apl_host text NULL,
	apl_port text NULL,
	apl_path_service text NULL
);

CREATE TABLE main.spb_service (
	srv_operacion_origen text NOT NULL,
	srv_operacion_servicio text NOT NULL,
	srv_formato text NULL,
	srv_path_carga text NULL,
	srv_apl_id_origen text NULL,
	srv_metodo_http text NULL,
	srv_apl_id_destino text NULL,
	srv_apl_operacion_destino text NULL,
	srv_apl_name_service_soap text NULL,
	srv_apl_port_service_soap text NULL,
	srv_param_type text NULL,
	srv_tipo_protocolo_http text NULL,
	srv_tipo_respuesta text NULL,
	srv_headers_request text NULL
);

CREATE TABLE main.spb_transformation (
	tra_id text NULL,
	tra_nombre text NULL,
	tra_apl_id text NULL,
	tra_operacion_origen text NULL,
	tra_nombre_proveedor text NOT NULL
);