USE [master]
GO

/****** Object:  Database [spb_bus]    Script Date: 07/04/2018 4:09:18 p. m. ******/
CREATE DATABASE [spb_bus]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'spb_bus', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\spb_bus.mdf' , SIZE = 524288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'spb_bus_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\spb_bus_log.ldf' , SIZE = 262144KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO

ALTER DATABASE [spb_bus] SET COMPATIBILITY_LEVEL = 140
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [spb_bus].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [spb_bus] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [spb_bus] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [spb_bus] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [spb_bus] SET ANSI_WARNINGS OFF 
GO
