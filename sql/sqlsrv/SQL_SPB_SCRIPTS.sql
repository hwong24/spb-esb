/************************** TABLAS  ************/ 
--CREACION TABLAS
USE [spb_bus]
-- GO

IF OBJECT_ID('spb_aplication', 'U') IS NOT NULL 
   drop table [dbo].[spb_aplication];

IF OBJECT_ID('[spb_service]', 'U') IS NOT NULL 
	drop table [dbo].[spb_service];

IF OBJECT_ID('[spb_transformation]', 'U') IS NOT NULL 
	drop table [dbo].[spb_transformation];


CREATE TABLE [dbo].[spb_aplication](
	[apl_id] [nchar](10) NOT NULL,
	[apl_nombre] [nchar](128) NULL,
	[apl_host] [nchar](128) NULL,
	[apl_port] [nchar](128) NULL,
	[apl_path_service] [nchar](128) NULL
) ON [PRIMARY]
-- GO


CREATE TABLE [dbo].[spb_service](
	[srv_operacion_origen] [nchar](128) NOT NULL,
	[srv_operacion_servicio] [nchar](128) NOT NULL,
	[srv_formato] [nchar](128) NULL,
	[srv_path_carga] [nchar](256) NULL,
	[srv_apl_id_origen] [nchar](128) NULL,
	[srv_metodo_http] [nchar](256) NULL,
	[srv_apl_id_destino] [nchar](256) NULL,
	[srv_apl_operacion_destino] [nchar](256) NULL,
	[srv_apl_name_service_soap] [nchar](256) NULL,
	[srv_apl_port_service_soap] [nchar](256) NULL,
	[srv_param_type] [nchar](10) NULL,
	[srv_tipo_protocolo_http] [nchar](10) NULL,
	[srv_tipo_respuesta] [nchar](10) NULL,
	[srv_headers_request] [nvarchar](255) NULL
) ON [PRIMARY]
-- GO

CREATE TABLE [dbo].[spb_transformation](
	[tra_id] [nchar](10) NULL,
	[tra_nombre] [nchar](128) NULL,
	[tra_apl_id] [nchar](128) NULL,
	[tra_operacion_origen] [nchar](128) NULL,
	[tra_nombre_proveedor] [nchar](128) NOT NULL
) ON [PRIMARY]
-- GO


