package org.spb.model.request;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class RequestCargaArrayToString extends AbstractMessageTransformer {

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		//ArrayList/LinkedHashMap
		LinkedHashMap<Object,Object> hm= new LinkedHashMap<Object,Object>();
		ArrayList<Object> al= new ArrayList<Object>();
		//String jsonPayload = "";
		//System.out.println("TIPO OBJECTO2: " + message.getInvocationProperty("stringPayload").getClass().getSimpleName());
		//String transformacion=message.getInvocationProperty("srv_transformacion").toString();
		String jsonPayload = message.getInvocationProperty("stringPayload");
		JsonParser jsonParser = new JsonParser();
		  JsonObject objectFromString = jsonParser.parse(jsonPayload).getAsJsonObject();
		  System.out.println("objectFromString" + objectFromString.toString());
		 
		if("LinkedHashMap".equalsIgnoreCase(message.getInvocationProperty("stringPayload").getClass().getSimpleName())){
			  
			hm  =  message.getInvocationProperty("stringPayload");
			
			for(Entry<Object, Object> m:hm.entrySet()){  
				   //System.out.println("KEY: " + m.getKey()+"VALUE: " + m.getValue());  
				   if ("carga".equalsIgnoreCase(m.getKey().toString())){
					   jsonPayload = m.getValue().toString();
				   }
			} 
		}
		else if("ArrayList".equalsIgnoreCase(message.getInvocationProperty("stringPayload").getClass().getSimpleName())){
			Iterator<Object> nombreIterator = al.iterator();
			al = message.getInvocationProperty("stringPayload");
			
			while(nombreIterator.hasNext()){
				String elemento = (String)nombreIterator.next();
				if("carga".equalsIgnoreCase(elemento)){
					jsonPayload = elemento;
				}
			}
		}
		//String jsonPayload = message.getInvocationProperty("stringPayload");
		
		if(!jsonPayload.isEmpty()){
			//JsonParser jsonParser = new JsonParser();
			//JsonObject objectFromString = jsonParser.parse(jsonPayload).getAsJsonObject();
			//System.out.println("Payload: " + objectFromString.toString());
			//transformCargaParamArrayToString(message,transformacion,objectFromString);
			
		}
		else{
			message.setInvocationProperty("carga", "");
		}
		
		//System.out.println("TranformCarga-------->"+message.getInvocationProperty("carga"));
		return message.getPayload();
	}
	
	public void transformCargaParamArrayToString(MuleMessage message,String transformacion, JsonObject objectFromString){
		String ArrayString ="";
		switch(transformacion){
		case "TRX05":
			ArrayString= objectFromString.get("ListaAdjuntos").toString();
			message.setInvocationProperty("ListaAdjuntos", objectFromString.get("ListaAdjuntos").toString());
			System.out.println("ListaAdjuntos: " + ArrayString);
			break;
		
		default :
			break;
		}
		
	}
}
