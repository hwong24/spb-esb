package org.spb.model.request;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.ArrayList;

public class RequestDetailTransform extends AbstractMessageTransformer {

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		//ArrayList/LinkedHashMap
		LinkedHashMap<Object,Object> hm= new LinkedHashMap<Object,Object>();
		ArrayList<Object> al= new ArrayList<Object>();
		String jsonPayload = "";
		System.out.println("TIPO OBJECTO: " + message.getInvocationProperty("stringPayload").getClass().getSimpleName());
		String tmpNombreItem="";
		if("LinkedHashMap".equalsIgnoreCase(message.getInvocationProperty("stringPayload").getClass().getSimpleName())){
			  
			hm  =  message.getInvocationProperty("stringPayload");
			
			for(Entry<Object, Object> m:hm.entrySet()){  
				   //System.out.println("KEY: " + m.getKey()+"VALUE: " + m.getValue());  
					
					if ("carga".equalsIgnoreCase(m.getKey().toString())){   
						tmpNombreItem = m.getKey().toString();
						jsonPayload = m.getValue().toString();
				   }
				   else if ("ListaAdjuntos".equalsIgnoreCase(m.getKey().toString())){
					   tmpNombreItem = m.getKey().toString();
					   jsonPayload = m.getValue().toString();
					   //System.out.println("JSONPAYLOAD: " + jsonPayload); 
				   }
			} 
		}
		else if("ArrayList".equalsIgnoreCase(message.getInvocationProperty("stringPayload").getClass().getSimpleName())){
			Iterator<Object> nombreIterator = al.iterator();
			al = message.getInvocationProperty("stringPayload");
			
			while(nombreIterator.hasNext()){
				String elemento = (String)nombreIterator.next();
				if("carga".equalsIgnoreCase(elemento)){
					jsonPayload = elemento;
				}
			}
		}
		//String jsonPayload = message.getInvocationProperty("stringPayload");
		
		if(!jsonPayload.isEmpty()){
			JsonParser jsonParser = new JsonParser();
			JsonElement jsonElement = jsonParser.parse(jsonPayload);
			JsonObject jsonObject = new JsonObject();
			JsonArray jsonArray = new JsonArray();
			
			if (jsonElement instanceof JsonObject) {
			    jsonObject = jsonElement.getAsJsonObject();
			    System.out.println("OBJECT");
			 } 
			else if (jsonElement instanceof JsonArray) {
			    jsonArray = jsonElement.getAsJsonArray();
			    System.out.println("ARRAY" + jsonArray.toString());
			}
						
			if ("carga".equalsIgnoreCase(tmpNombreItem)){
				message.setInvocationProperty("carga", jsonObject.get("carga").toString());
			}
			if ("ListaAdjuntos".equalsIgnoreCase(tmpNombreItem)){
				message.setInvocationProperty("ListaAdjuntos", jsonArray.toString());
			}
		}
		else{
			message.setInvocationProperty("carga", "");
		}
		System.out.println("TranformCarga-------->"+message.getInvocationProperty("ListaAdjuntos"));
		return message.getPayload();
	}
	

}