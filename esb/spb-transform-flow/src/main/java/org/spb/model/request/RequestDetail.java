package org.spb.model.request;

import java.util.HashMap;
import java.util.Objects;

import org.codehaus.jackson.annotate.JsonAutoDetect;

@JsonAutoDetect
public class RequestDetail {
	
	private String srv_formato;
	private String srv_path_carga;
	private String srv_operacion_destino;
	private String srv_metodo_http;
	private String srv_endpoint;
	private String srv_operacion_origen;
	private String srv_nombre_proveedor;	
	private String srv_transformacion;
	private String srv_operacion_servicio;
	private String srv_entidad;
	//private HashMap<String, Object> carga = new HashMap<>(); 
	private String srv_apl_port_service_soa;
	


	private String srv_apl_name_service_soa;
	private String srv_host;
	private String srv_port;
	private String srv_path_service_request;
	private String srv_param_type;
	private String srv_tipo_protocolo_http;
	private String srv_tipo_respuesta;
	private String srv_apl_id_destino;
	private String srv_apl_id_origen;
	private String srv_headers_request;
	private String correlationID;
	
	/*srv_formato,             srv_path_carga,           srv_apl_id_destino,  
    srv_operacion_destino,   srv_metodo_http,          srv_endpoint,               
    srv_operacion_servicio,  srv_apl_id_origen,        srv_operacion_origen,     
    srv_nombre_provedor,    srv_transformacion,       srv_apl_port_service_soa, 
    srv_apl_name_service_soa,srv_host, srv_port,       srv_path_service_request, 
    srv_param_type,          srv_tipo_protocolo_http,  srv_tipo_respuesta*/ 

	public String getSrv_formato() {
		return srv_formato;
	}


	public void setSrv_formato(String srv_formato) {
		this.srv_formato = srv_formato;
	}


	public String getSrv_path_carga() {
		return srv_path_carga;
	}


	public void setSrv_path_carga(String srv_path_carga) {
		this.srv_path_carga = srv_path_carga;
	}


	public String getSrv_operacion_destino() {
		return srv_operacion_destino;
	}


	public void setSrv_operacion_destino(String srv_operacion_destino) {
		this.srv_operacion_destino = srv_operacion_destino;
	}


	public String getSrv_metodo_http() {
		return srv_metodo_http;
	}


	public void setSrv_metodo_http(String srv_metodo_http) {
		this.srv_metodo_http = srv_metodo_http;
	}


	public String getSrv_endpoint() {
		return srv_endpoint;
	}


	public void setSrv_endpoint(String srv_endpoint) {
		this.srv_endpoint = srv_endpoint;
	}



	public String getSrv_operacion_origen() {
		return srv_operacion_origen;
	}



	public void setSrv_operacion_origen(String srv_operacion_origen) {
		this.srv_operacion_origen = srv_operacion_origen;
	}


	public String getSrv_nombre_proveedor() {
		return srv_nombre_proveedor;
	}


	public void setSrv_nombre_provedor(String srv_nombre_proveedor) {
		this.srv_nombre_proveedor = srv_nombre_proveedor;
	}

		
	public String getSrv_transformacion() {
		return srv_transformacion;
	}


	public void setSrv_transformacion(String srv_transformacion) {
		this.srv_transformacion = srv_transformacion;
	}
	
	
	public String getSrv_operacion_servicio() {
		return srv_operacion_servicio;
	}


	public void setSrv_operacion_servicio(String srv_operacion_servicio) {
		this.srv_operacion_servicio = srv_operacion_servicio;
	}

	public String getSrv_entidad() {
		return srv_entidad;
	}


	public void setSrv_entidad(String srv_entidad) {
		this.srv_entidad = srv_entidad;
	}


	public String getSrv_apl_port_service_soa() {
		return srv_apl_port_service_soa;
	}


	public void setSrv_apl_port_service_soa(String srv_apl_port_service_soa) {
		this.srv_apl_port_service_soa = srv_apl_port_service_soa;
	}


	public String getSrv_apl_name_service_soa() {
		return srv_apl_name_service_soa;
	}


	public void setSrv_apl_name_service_soa(String srv_apl_name_service_soa) {
		this.srv_apl_name_service_soa = srv_apl_name_service_soa;
	}


	public String getSrv_host() {
		return srv_host;
	}


	public void setSrv_host(String srv_host) {
		this.srv_host = srv_host;
	}


	public String getSrv_port() {
		return srv_port;
	}


	public void setSrv_port(String srv_port) {
		this.srv_port = srv_port;
	}


	public String getSrv_path_service_request() {
		return srv_path_service_request;
	}


	public void setSrv_path_service_request(String srv_path_service_request) {
		this.srv_path_service_request = srv_path_service_request;
	}
	
	public String getSrv_param_type() {
		return srv_param_type;
	}


	public void setSrv_param_type(String srv_param_type) {
		this.srv_param_type = srv_param_type;
	}


	public String getSrv_tipo_respuesta() {
		return srv_tipo_respuesta;
	}


	public void setSrv_tipo_respuesta(String srv_tipo_respuesta) {
		this.srv_tipo_respuesta = srv_tipo_respuesta;
	}
	
	public String getSrv_tipo_protocolo_http() {
		return srv_tipo_protocolo_http;
	}


	public void setSrv_tipo_protocolo_http(String srv_tipo_protocolo_http) {
		this.srv_tipo_protocolo_http = srv_tipo_protocolo_http;
	}


	public String getSrv_apl_id_destino() {
		return srv_apl_id_destino;
	}


	public void setSrv_apl_id_destino(String srv_apl_id_destino) {
		this.srv_apl_id_destino = srv_apl_id_destino;
	}


	public String getSrv_apl_id_origen() {
		return srv_apl_id_origen;
	}


	public void setSrv_apl_id_origen(String srv_apl_id_origen) {
		this.srv_apl_id_origen = srv_apl_id_origen;
	}


	public void setSrv_nombre_proveedor(String srv_nombre_proveedor) {
		this.srv_nombre_proveedor = srv_nombre_proveedor;
	}
	
	public String getSrv_headers_request() {
		return srv_headers_request;
	}


	public void setSrv_headers_request(String srv_headers_request) {
		this.srv_headers_request = srv_headers_request;
	}

	public String getCorrelationID() {
		return correlationID;
	}


	public void setCorrelationID(String correlationID) {
		this.correlationID = correlationID;
	}
	
	@Override
	public String toString() {
		return "RequestDetail [srv_formato=" + srv_formato + ", srv_path_carga=" + srv_path_carga
				+ ", srv_operacion_destino=" + srv_operacion_destino + ", srv_metodo_http=" + srv_metodo_http
				+ ", srv_endpoint=" + srv_endpoint + ", srv_operacion_origen=" + srv_operacion_origen
				+ ", srv_nombre_proveedor=" + srv_nombre_proveedor + ", srv_transformacion=" + srv_transformacion
				+ ", srv_operacion_servicio=" + srv_operacion_servicio + ", srv_entidad=" + srv_entidad
				+ ", srv_apl_port_service_soa=" + srv_apl_port_service_soa + ", srv_apl_name_service_soa="
				+ srv_apl_name_service_soa + ", srv_host=" + srv_host + ", srv_port=" + srv_port
				+ ", srv_path_service_request=" + srv_path_service_request + ", srv_param_type=" + srv_param_type
				+ ", srv_tipo_protocolo_http=" + srv_tipo_protocolo_http + ", srv_tipo_respuesta=" + srv_tipo_respuesta
				+ ", srv_apl_id_destino=" + srv_apl_id_destino + ", srv_apl_id_origen=" + srv_apl_id_origen
				+ ", srv_headers_request=" + srv_headers_request + ", correlationID=" + correlationID + "]";
	}
}
