<?xml version="1.0" encoding="UTF-8"?>

<mule xmlns:mulexml="http://www.mulesoft.org/schema/mule/xml" xmlns:jms="http://www.mulesoft.org/schema/mule/jms" xmlns:tracking="http://www.mulesoft.org/schema/mule/ee/tracking" xmlns:vm="http://www.mulesoft.org/schema/mule/vm" xmlns:metadata="http://www.mulesoft.org/schema/mule/metadata" xmlns:json="http://www.mulesoft.org/schema/mule/json"
	xmlns:http="http://www.mulesoft.org/schema/mule/http"
	xmlns:dw="http://www.mulesoft.org/schema/mule/ee/dw"
	xmlns="http://www.mulesoft.org/schema/mule/core" xmlns:doc="http://www.mulesoft.org/schema/mule/documentation"
	xmlns:spring="http://www.springframework.org/schema/beans" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.mulesoft.org/schema/mule/json http://www.mulesoft.org/schema/mule/json/current/mule-json.xsd
http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd
http://www.mulesoft.org/schema/mule/ee/dw http://www.mulesoft.org/schema/mule/ee/dw/current/dw.xsd
http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-current.xsd
http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd
http://www.mulesoft.org/schema/mule/vm http://www.mulesoft.org/schema/mule/vm/current/mule-vm.xsd
http://www.mulesoft.org/schema/mule/ee/tracking http://www.mulesoft.org/schema/mule/ee/tracking/current/mule-tracking-ee.xsd
http://www.mulesoft.org/schema/mule/jms http://www.mulesoft.org/schema/mule/jms/current/mule-jms.xsd
http://www.mulesoft.org/schema/mule/xml http://www.mulesoft.org/schema/mule/xml/current/mule-xml.xsd">
    <jms:activemq-connector name="Active_MQ" brokerURL="tcp://localhost:61616" validateConnections="true" doc:name="Active MQ"/>
    <http:listener-config name="HTTP_Listener_Configuration" host="localhost" port="8082" doc:name="HTTP Listener Configuration"/>
    
    <flow name="test-transform-flowFlow">
        <http:listener config-ref="HTTP_Listener_Configuration" path="/transform" allowedMethods="POST" doc:name="HTTP"/>
        <logger message="#['Inicio transformacion --------------------------&gt;     ']#[payload]" level="INFO" doc:name="Inbound Logger"/>
        <json:json-to-object-transformer returnClass="org.spb.model.request.RequestDetail" doc:name="JSON to Object" mimeType="application/java"/>
        <set-variable variableName="Set RequestDetail" value="#[flowVars.request = payload]" doc:name="Request Payload"/>
        <choice doc:name="Choice">
            <when expression="#[payload.srv_transformacion == 'TRX01']">
                <dw:transform-message doc:name="Message TRX01" metadata:id="85a9433c-2914-49b7-bb15-d29b89f02e0c">
                    <dw:input-payload doc:sample="sample_data\RequestDetail.dwl" mimeType="application/java"/>
                    <dw:set-payload><![CDATA[%dw 1.0
%output application/json
---
{
	endpoint: payload.srv_endpoint,
	formato: payload.srv_formato,
	metodo: payload.srv_metodo_http,
	metodoServicio: payload.srv_operacion_servicio,
	entidadSeleccionada: payload.srv_entidad,
	carga: payload.carga,
	host: payload.srv_host,
	port: payload.srv_port,
	path_service_request: payload.srv_path_service_request,
	(payload.srv_entidad): payload.carga,
	paramType:payload.srv_param_type,
	tipoProtocolo:payload.srv_tipo_protocolo_http,
	tipoRespuesta:payload.srv_tipo_respuesta
}]]></dw:set-payload>
                </dw:transform-message>
            </when>
            <when expression="#[payload.srv_transformacion == 'TRX02']">
                <dw:transform-message doc:name="Message TRX02" metadata:id="3f58cdd8-6bb4-4e21-bdbd-d2686ec14134">
                    <dw:input-payload mimeType="application/java"/>
                    <dw:set-payload><![CDATA[%dw 1.0
%output application/json
---
{
	endpoint: payload.srv_endpoint,
	formato: payload.srv_formato,
	metodo: payload.srv_metodo_http,
	metodoServicio: payload.srv_operacion_servicio,
	entidadSeleccionada: payload.srv_entidad,
	carga: payload.carga,
	host: payload.srv_host,
	port: payload.srv_port,
	path_service_request: payload.srv_path_service_request,
	(payload.srv_entidad): payload.carga,
	paramType:payload.srv_param_type,
	tipoProtocolo:payload.srv_tipo_protocolo_http,
	tipoRespuesta:payload.srv_tipo_respuesta
	
}]]></dw:set-payload>
                </dw:transform-message>
            </when>
            <when expression="#[payload.srv_transformacion == 'TRX03']">
            <dw:transform-message doc:name="Message TRX03" metadata:id="be92a3ce-2dd3-4233-bd18-bbdf05dea7c4">
                    <dw:set-payload><![CDATA[%dw 1.0
%output application/json
---
{
 endpoint: payload.srv_endpoint,
 formato: payload.srv_formato,
 metodo: payload.srv_metodo_http,
 metodoServicio: payload.srv_operacion_servicio,
 host: payload.srv_host,
 port: payload.srv_port,
 paramType:payload.srv_param_type,
 tipoProtocolo:payload.srv_tipo_protocolo_http,
 tipoRespuesta:payload.srv_tipo_respuesta
}]]></dw:set-payload>
            </dw:transform-message>
            </when>
            <when expression="#[payload.srv_transformacion == 'TRX04']">
                <dw:transform-message doc:name="Message TRX04" metadata:id="96444fd3-7f27-40f6-b7c7-a25e87cd9049">
                    <dw:set-payload><![CDATA[%dw 1.0
%output application/xml
%namespace ns0 http://spb.com/courses
%namespace ns01 http://schemas.xmlsoap.org/soap/envelope/
---
{
	ns01#Envelope: {
		ns01#Body: {
			ns0#GetCourseDetailsRequest: {
				ns0#id: flowVars.request.carga.id
			}
		}
	}
}]]></dw:set-payload>
                </dw:transform-message>
                <byte-array-to-object-transformer returnClass="java.lang.Object" mimeType="application/java" doc:name="Byte Array to Object"/>
                <dw:transform-message doc:name="Transform Message" metadata:id="af540513-cdda-4742-a884-3136343cdafe">
                    <dw:set-payload><![CDATA[%dw 1.0
%output application/json
---
{
	formato: flowVars.request.srv_formato,
	serviceSrcPath: flowVars.request.srv_path_carga,
	metodo: flowVars.request.srv_metodo_http,
	endpoint: flowVars.request.srv_endpoint,
	metodoServicio: flowVars.request.srv_operacion_servicio,
	portServiceSoa: flowVars.request.srv_apl_port_service_soa,
	nameServiceSoa: flowVars.request.srv_apl_name_service_soa,
	entidadSeleccionada: flowVars.request.srv_entidad,
	(flowVars.request.srv_entidad): payload,
	host: flowVars.request.srv_host,
    port: flowVars.request.srv_port,
    tipoProtocolo:flowVars.request.srv_tipo_protocolo_http,
    tipoRespuesta:payload.srv_tipo_respuesta
}]]></dw:set-payload>
                </dw:transform-message>
            </when>
            <when expression="#[payload.srv_transformacion == 'TRX05']">
                <dw:transform-message doc:name="Message TRX05" metadata:id="814c648d-688e-423a-92f4-4487cb5529f8">
                    <dw:set-payload><![CDATA[%dw 1.0
%output application/xml
%namespace ns0 http://schemas.xmlsoap.org/soap/envelope/
%namespace ns1 https://graphical.weather.gov/xml/DWMLgen/wsdl/ndfdXML.wsdl
---
{
	ns0#Envelope: {
		ns0#Body: {
			ns1#LatLonListZipCode: {
				zipCodeList: flowVars.request.carga.zipCodeList
			}
		}
	}
}]]></dw:set-payload>
                </dw:transform-message>
                <byte-array-to-object-transformer returnClass="java.lang.Object" mimeType="application/java" doc:name="Byte Array to Object"/>
                <dw:transform-message doc:name="Transform Message">
                    <dw:set-payload><![CDATA[%dw 1.0
%output application/json
---
{
	formato: flowVars.request.srv_formato,
	serviceSrcPath: flowVars.request.srv_path_carga,
	metodo: flowVars.request.srv_metodo_http,
	endpoint: flowVars.request.srv_endpoint,
	metodoServicio: flowVars.request.srv_operacion_servicio,
	portServiceSoa: flowVars.request.srv_apl_port_service_soa,
	nameServiceSoa: flowVars.request.srv_apl_name_service_soa,
	entidadSeleccionada: flowVars.request.srv_entidad,
	(flowVars.request.srv_entidad): payload,
	host: flowVars.request.srv_host,
    port: flowVars.request.srv_port,
    tipoProtocolo:flowVars.request.srv_tipo_protocolo_http
}]]></dw:set-payload>
                </dw:transform-message>
            </when>
            <when expression="#[payload.srv_transformacion == 'TRX06']">
                <dw:transform-message doc:name="Message TRX06" metadata:id="af1d8769-4b1e-4141-a582-cfa379a2bdc1">
                    <dw:set-payload><![CDATA[%dw 1.0
%output application/xml
%namespace ns0 http://www.w3.org/2003/05/soap-envelope
%namespace ns1 http://wsDoc4Us
---
{
	ns0#Envelope: {
		ns0#Body: {
			ns1#ConsultarDependencias: {
				ns1#user: flowVars.request.carga.user,
				ns1#password: flowVars.request.carga.password,
				ns1#idEmpresa_: flowVars.request.carga.idEmpresa_
			}
		}
	}
}]]></dw:set-payload>
                </dw:transform-message>
                <byte-array-to-object-transformer returnClass="java.lang.Object" mimeType="application/java" doc:name="Byte Array to Object"/>
                <dw:transform-message doc:name="Transform Message">
                    <dw:set-payload><![CDATA[%dw 1.0
%output application/json
---
{
	formato: flowVars.request.srv_formato,
	serviceSrcPath: flowVars.request.srv_path_carga,
	metodo: flowVars.request.srv_metodo_http,
	endpoint: flowVars.request.srv_endpoint,
	metodoServicio: flowVars.request.srv_operacion_servicio,
	portServiceSoa: flowVars.request.srv_apl_port_service_soa,
	nameServiceSoa: flowVars.request.srv_apl_name_service_soa,
	entidadSeleccionada: flowVars.request.srv_entidad,
	(flowVars.request.srv_entidad): payload,
	host: flowVars.request.srv_host,
    port: flowVars.request.srv_port,
    tipoProtocolo:flowVars.request.srv_tipo_protocolo_http
}]]></dw:set-payload>
                </dw:transform-message>
            </when>
            <otherwise>
                <dw:transform-message doc:name="Default TRX" metadata:id="e2d7888c-7718-44aa-8024-c12df2e39bca">
                    <dw:input-payload mimeType="application/java"/>
                    <dw:set-payload><![CDATA[%dw 1.0
%output application/json
---
{
	endpoint: payload.srv_endpoint,
	formato: payload.srv_formato,
	metodo: payload.srv_metodo_http,
	metodoServicio: payload.srv_operacion_servicio,
	carga: payload.carga	
}]]></dw:set-payload>
                </dw:transform-message>
            </otherwise>
        </choice>
        <object-to-string-transformer mimeType="application/json" doc:name="Object to String"/>
        <logger message="#[payload]" level="INFO" doc:name="Outbound Logger"/>

    </flow>
</mule>
