package com.spb.response;

import java.io.Serializable;
import java.util.HashMap;

public class ResponseDetail implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6624585448430725516L;
	private String endpoint;
	private String formato;
	private String metodo;
	private String metodoServicio;
	private String entidadSeleccionada;
	private String portServiceSoa;
	private String nameServiceSoa;
	private String serviceSrcPath;
	private String host;
	private String port;
	private String path_service_request;

	private HashMap<String, String> carga;
	private HashMap<String, String> cliente;
	private HashMap<String, String> producto;
	private HashMap<String, String> users;	
	private String course;	
	
	
	public ResponseDetail(){}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	public String getMetodo() {
		return metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

		
	public HashMap<String, String> getCarga() {
		return carga;
	}

	public void setCarga(HashMap<String, String> carga) {
		this.carga = carga;
	}

	public String getMetodoServicio() {
		return metodoServicio;
	}

	public void setMetodoServicio(String metodoServicio) {
		this.metodoServicio = metodoServicio;
	}

	public HashMap<String, String> getCliente() {
		return cliente;
	}

	public void setCliente(HashMap<String, String> cliente) {
		this.cliente = cliente;
	}

	public String getEntidadSeleccionada() {
		return entidadSeleccionada;
	}

	public void setEntidadSeleccionada(String entidadSeleccionada) {
		this.entidadSeleccionada = entidadSeleccionada;
	}

	public HashMap<String, String> getProducto() {
		return producto;
	}

	public void setProducto(HashMap<String, String> producto) {
		this.producto = producto;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public String getPortServiceSoa() {
		return portServiceSoa;
	}

	public void setPortServiceSoa(String portServiceSoa) {
		this.portServiceSoa = portServiceSoa;
	}

	public String getNameServiceSoa() {
		return nameServiceSoa;
	}

	public void setNameServiceSoa(String nameServiceSoa) {
		this.nameServiceSoa = nameServiceSoa;
	}

	public String getServiceSrcPath() {
		return serviceSrcPath;
	}

	public void setServiceSrcPath(String serviceSrcPath) {
		this.serviceSrcPath = serviceSrcPath;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getPath_service_request() {
		return path_service_request;
	}

	public void setPath_service_request(String path_service_request) {
		this.path_service_request = path_service_request;
	}

	public HashMap<String, String> getUsers() {
		return users;
	}

	public void setUsers(HashMap<String, String> users) {
		this.users = users;
	}

	@Override
	public String toString() {
		return "ResponseDetail [endpoint=" + endpoint + ", formato=" + formato + ", metodo=" + metodo
				+ ", metodoServicio=" + metodoServicio + ", entidadSeleccionada=" + entidadSeleccionada
				+ ", portServiceSoa=" + portServiceSoa + ", nameServiceSoa=" + nameServiceSoa + ", serviceSrcPath="
				+ serviceSrcPath + ", host=" + host + ", port=" + port + ", path_service_request="
				+ path_service_request + ", carga=" + carga + ", cliente=" + cliente + ", producto=" + producto
				+ ", users=" + users + ", course=" + course + "]";
	}

}
