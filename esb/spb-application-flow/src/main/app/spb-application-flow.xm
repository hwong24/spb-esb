<?xml version="1.0" encoding="UTF-8"?>

<mule xmlns:jms="http://www.mulesoft.org/schema/mule/jms" xmlns:metadata="http://www.mulesoft.org/schema/mule/metadata" xmlns:dw="http://www.mulesoft.org/schema/mule/ee/dw" xmlns:http="http://www.mulesoft.org/schema/mule/http" xmlns:json="http://www.mulesoft.org/schema/mule/json" xmlns:db="http://www.mulesoft.org/schema/mule/db" xmlns:vm="http://www.mulesoft.org/schema/mule/vm" xmlns="http://www.mulesoft.org/schema/mule/core" xmlns:doc="http://www.mulesoft.org/schema/mule/documentation"
	xmlns:spring="http://www.springframework.org/schema/beans" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-current.xsd
http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd
http://www.mulesoft.org/schema/mule/json http://www.mulesoft.org/schema/mule/json/current/mule-json.xsd
http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd
http://www.mulesoft.org/schema/mule/db http://www.mulesoft.org/schema/mule/db/current/mule-db.xsd
http://www.mulesoft.org/schema/mule/ee/dw http://www.mulesoft.org/schema/mule/ee/dw/current/dw.xsd
http://www.mulesoft.org/schema/mule/vm http://www.mulesoft.org/schema/mule/vm/current/mule-vm.xsd
http://www.mulesoft.org/schema/mule/jms http://www.mulesoft.org/schema/mule/jms/current/mule-jms.xsd">
    <db:mysql-config name="MySQL_Configuration" host="localhost" port="3306" user="root" password="root" database="db_spb" doc:name="MySQL Configuration">
        <reconnect-forever/>
    </db:mysql-config>
    <http:listener-config name="HTTP_Listener_Configuration" host="0.0.0.0" port="8081" doc:name="HTTP Listener Configuration"/>
    <flow name="conectbdFlow">
        <http:listener config-ref="HTTP_Listener_Configuration" path="/application" doc:name="HTTP"/>
        <json:json-to-object-transformer returnClass="java.lang.Object" doc:name="JSON to Object"/>
        <set-variable variableName="carga" value="#[payload.carga]" doc:name="Carga Params"/>
        <db:select config-ref="MySQL_Configuration" doc:name="Query Service Params">
            <db:dynamic-query><![CDATA[select * from spb_service where srv_operacion_origen = "#[message.payload.oper_origen]"]]></db:dynamic-query>
        </db:select>
        <dw:transform-message doc:name="Transform Message" metadata:id="1f8e07fe-38ce-4ced-b9de-338637c0fae0">
            <dw:set-payload><![CDATA[%dw 1.0
%output application/json
%var carga = flowVars.carga
---
{
	srv_formato: "REST",
	srv_path_carga: "/json",
	srv_operacion_destino: "consultaCliente",
	srv_metodo_http: "POST",
	srv_endpoint: "http://localhost:8080",
	srv_operacion_servicio: "/users/",
	srv_operacion_origen: "buscarCliente",
	srv_nombre_provedor: "SAP",
	srv_transformacion: "TRX02",
	carga: carga
}]]></dw:set-payload>
        </dw:transform-message>
        
        <logger message="#['Datos BD:  ' + payload] " level="INFO" doc:name="Logger"/>
        <vm:outbound-endpoint exchange-pattern="one-way" doc:name="VM" connector-ref="DomainAppSPB" path="SPB_QUEUE_REQUEST"/>
    </flow>
</mule>
