<?xml version="1.0" encoding="UTF-8"?>

<mule xmlns:ws="http://www.mulesoft.org/schema/mule/ws" xmlns:mulexml="http://www.mulesoft.org/schema/mule/xml" xmlns:tracking="http://www.mulesoft.org/schema/mule/ee/tracking" xmlns:jms="http://www.mulesoft.org/schema/mule/jms" xmlns:metadata="http://www.mulesoft.org/schema/mule/metadata"
	xmlns:dw="http://www.mulesoft.org/schema/mule/ee/dw" xmlns:http="http://www.mulesoft.org/schema/mule/http"
	xmlns:json="http://www.mulesoft.org/schema/mule/json" xmlns:db="http://www.mulesoft.org/schema/mule/db"
	xmlns:vm="http://www.mulesoft.org/schema/mule/vm" xmlns="http://www.mulesoft.org/schema/mule/core"
	xmlns:doc="http://www.mulesoft.org/schema/mule/documentation"
	xmlns:spring="http://www.springframework.org/schema/beans" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-current.xsd
http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd
http://www.mulesoft.org/schema/mule/json http://www.mulesoft.org/schema/mule/json/current/mule-json.xsd
http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd
http://www.mulesoft.org/schema/mule/db http://www.mulesoft.org/schema/mule/db/current/mule-db.xsd
http://www.mulesoft.org/schema/mule/ee/dw http://www.mulesoft.org/schema/mule/ee/dw/current/dw.xsd
http://www.mulesoft.org/schema/mule/vm http://www.mulesoft.org/schema/mule/vm/current/mule-vm.xsd
http://www.mulesoft.org/schema/mule/jms http://www.mulesoft.org/schema/mule/jms/current/mule-jms.xsd
http://www.mulesoft.org/schema/mule/ee/tracking http://www.mulesoft.org/schema/mule/ee/tracking/current/mule-tracking-ee.xsd
http://www.mulesoft.org/schema/mule/xml http://www.mulesoft.org/schema/mule/xml/current/mule-xml.xsd
http://www.mulesoft.org/schema/mule/ws http://www.mulesoft.org/schema/mule/ws/current/mule-ws.xsd">
<jms:activemq-connector name="Active_MQ" brokerURL="tcp://localhost:61616" validateConnections="true" doc:name="Active MQ"/>
    <http:request-config name="HTTP_Request_Configuration" doc:name="HTTP Request Configuration" host="localhost" port="8082"/>
    <ws:consumer-config name="Web_Service_Consumer" wsdlLocation="services/courseDetails/courses.wsdl" service="CoursePortService" port="CoursePortSoap11" serviceAddress="#[payload.endpoint]" doc:name="Web Service Consumer"/>
    <http:request-config name="HTTP_Rest_Request_Config" host="#[payload.host]" port="#[payload.port]" doc:name="HTTP Request Configuration"/>
	<flow name="spb-jms-process-flow">
        <jms:inbound-endpoint queue="RESP_MF" doc:name="JMS" connector-ref="Active_MQ"/>
        <logger level="INFO" doc:name="Inbound Logger"/>
        <json:object-to-json-transformer doc:name="Object to JSON" mimeType="application/json"/>
        <json:json-to-object-transformer returnClass="java.util.HashMap" mimeType="application/java" doc:name="JSON to Object"/>
        <set-variable variableName="request" value="#[payload]" mimeType="application/java" doc:name="Variable"/>
        <logger message="#['------&gt;Datos de JMS:    '+payload]" level="INFO" doc:name="Inbound logger"/>
        <choice doc:name="Web Service protocol - choice">
            <when expression="#[payload.formato=='REST']">
                <choice doc:name="REST Http method - choice">
                    <when expression="#[payload.metodo == 'GET']">
                        <logger message="#['REST HTTP GET']" level="INFO" doc:name="Get request Logger"/>
                        <http:request config-ref="HTTP_Rest_Request_Config" path="#[payload.metodoServicio]" method="GET" doc:name="HTTP GET Request">
                            <http:request-builder>
                                <http:uri-params expression="#[payload.get(payload.get('entidadSeleccionada'))]"/>
                            </http:request-builder>
                        </http:request>
                    </when>                    
                    <otherwise>
                        <logger message="#['POST HTTP REST']" level="INFO" doc:name="Post Request Logger"/>
                        <set-payload value="#[payload.(payload.entidadSeleccionada)]" doc:name="Set Payload"/>
                        <http:request config-ref="HTTP_Rest_Request_Config" path="#[payload.metodo_servicio]" method="POST" doc:name="HTTP POST Request">
                            <http:request-builder>
                                <http:header headerName="Content-Type" value="application/json;charset=utf-8"/>
                            </http:request-builder>
                        </http:request>
                    </otherwise>
                </choice>
                <object-to-string-transformer doc:name="Object to String"/>
            </when>
            <otherwise>
                <logger level="INFO" doc:name="SOAP" message="#['DEFAULT '+payload]"/>
            </otherwise>
        </choice>
        <jms:outbound-endpoint queue="REPLY_QUEUE" connector-ref="Active_MQ" doc:name="JMS"/>
	</flow>
</mule>
