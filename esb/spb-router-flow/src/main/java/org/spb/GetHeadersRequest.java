package org.spb;

import java.util.HashMap;
import java.util.Objects;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;


public class GetHeadersRequest extends AbstractMessageTransformer {	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		
		HashMap<String,Object> payloadHeaders = new HashMap<String,Object>();
		payloadHeaders = (HashMap<String, Object>) message.getPayload();
		HashMap<String, String> mapHeaders = new HashMap<String,String>();
				
		String dataHeaders;
		dataHeaders = payloadHeaders.get("headersRequest").toString();
		System.out.println("dataHeaders--->"+dataHeaders);
		
		if(!(Objects.isNull(dataHeaders) || dataHeaders.isEmpty())){
			JSONArray jSONArray = new JSONArray(dataHeaders);
			for (int i = 0; i < jSONArray.length(); i++) {
				JSONObject jSONObject = (JSONObject) jSONArray.get(i);
				Set keySet = jSONObject.keySet();
				for (Object temp : keySet) {
			        mapHeaders.put(temp.toString(),jSONObject.get(temp.toString()).toString());
			     }
			}
			message.setInvocationProperty("headers", mapHeaders);
		}
		else {
			mapHeaders.put("","");
			System.out.println("mapHeaders Vacio--->");
		}
		System.out.println("mapHeaders--->"+mapHeaders);
		
		return message.getPayload();
	}
}
