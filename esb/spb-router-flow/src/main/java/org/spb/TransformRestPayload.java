package org.spb;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class TransformRestPayload  extends AbstractMessageTransformer {

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		String jsonPayload = message.getInvocationProperty("stringPayload");
		System.out.println("jsonPayload: " +jsonPayload);
 		JsonParser jsonParser = new JsonParser();
		JsonObject objectFromString = jsonParser.parse(jsonPayload).getAsJsonObject();
		System.out.println("objectFromString: " + objectFromString.toString());
		message.setInvocationProperty("carga", objectFromString.get("carga").toString());
		
		System.out.println("property-------->"+message.getInvocationProperty("carga"));
		return message.getPayload();
	}
}
