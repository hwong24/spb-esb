<?xml version="1.0" encoding="UTF-8"?>

<mule xmlns:scripting="http://www.mulesoft.org/schema/mule/scripting" xmlns:ws="http://www.mulesoft.org/schema/mule/ws" xmlns:mulexml="http://www.mulesoft.org/schema/mule/xml" xmlns:tracking="http://www.mulesoft.org/schema/mule/ee/tracking" xmlns:jms="http://www.mulesoft.org/schema/mule/jms" xmlns:metadata="http://www.mulesoft.org/schema/mule/metadata"
	xmlns:dw="http://www.mulesoft.org/schema/mule/ee/dw" xmlns:http="http://www.mulesoft.org/schema/mule/http"
	xmlns:json="http://www.mulesoft.org/schema/mule/json" xmlns:db="http://www.mulesoft.org/schema/mule/db"
	xmlns:vm="http://www.mulesoft.org/schema/mule/vm" xmlns="http://www.mulesoft.org/schema/mule/core"
	xmlns:doc="http://www.mulesoft.org/schema/mule/documentation"
	xmlns:spring="http://www.springframework.org/schema/beans" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-current.xsd
http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd
http://www.mulesoft.org/schema/mule/json http://www.mulesoft.org/schema/mule/json/current/mule-json.xsd
http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd
http://www.mulesoft.org/schema/mule/db http://www.mulesoft.org/schema/mule/db/current/mule-db.xsd
http://www.mulesoft.org/schema/mule/ee/dw http://www.mulesoft.org/schema/mule/ee/dw/current/dw.xsd
http://www.mulesoft.org/schema/mule/vm http://www.mulesoft.org/schema/mule/vm/current/mule-vm.xsd
http://www.mulesoft.org/schema/mule/jms http://www.mulesoft.org/schema/mule/jms/current/mule-jms.xsd
http://www.mulesoft.org/schema/mule/ee/tracking http://www.mulesoft.org/schema/mule/ee/tracking/current/mule-tracking-ee.xsd
http://www.mulesoft.org/schema/mule/xml http://www.mulesoft.org/schema/mule/xml/current/mule-xml.xsd
http://www.mulesoft.org/schema/mule/ws http://www.mulesoft.org/schema/mule/ws/current/mule-ws.xsd
http://www.mulesoft.org/schema/mule/scripting http://www.mulesoft.org/schema/mule/scripting/current/mule-scripting.xsd">
<jms:activemq-connector name="Active_MQ" brokerURL="tcp://localhost:61616" validateConnections="true" doc:name="Active MQ"/>
    <http:request-config name="HTTP_Request_Configuration" doc:name="HTTP Request Configuration" host="localhost" port="8082"/>
    <ws:consumer-config name="Web_Service_Consumer" wsdlLocation="services/courseDetails/courses.wsdl" service="CoursePortService" port="CoursePortSoap11" serviceAddress="#[payload.endpoint]" doc:name="Web Service Consumer"/>
    <http:request-config name="HTTP_Rest_Request_Config" host="#[flowVars.request.host]" port="#[flowVars.request.port]" doc:name="HTTP Request Configuration"/>
    <http:request-config name="HTTP_Redirect_Application_Config" host="localhost" port="8081" doc:name="HTTP Request Configuration"/>
    <http:request-config name="HTTP_SOAP_Request_Config" host="#[flowVars.request.host]" port="#[flowVars.request.port]" doc:name="HTTP Request Configuration" />
    <jms:activemq-connector name="Active_MQ_RESPONSE_FINAL" brokerURL="tcp://localhost:61616" validateConnections="true" doc:name="Active MQ"/>
	<flow name="spb-router-flow">
        <jms:inbound-endpoint queue="RESP_MF" doc:name="JMS" connector-ref="Active_MQ">
        	<copy-properties propertyName="*" />
        </jms:inbound-endpoint>
        <logger message="#['INICIO ROUTER--&gt;' + message.inboundProperties['ROUTER_TYPE']] + #[&quot;PAYLOAD INI ROUTER --&gt;&quot; +payload]" level="INFO" doc:name="Logger"/>
        <choice doc:name="Choice">
            <when expression="#[message.inboundProperties.'ROUTER_TYPE'==null]">
                <logger level="INFO" doc:name="Inbound Logger"/>
        <json:object-to-json-transformer doc:name="Object to JSON" mimeType="application/json"/>
        <json:json-to-object-transformer returnClass="java.util.HashMap" mimeType="application/java" doc:name="JSON to Object"/>
        <set-variable variableName="request" value="#[payload]" mimeType="application/java" doc:name="Variable Request"/>
        <logger message="#['------&gt;Datos de JMS:    '+payload]" level="INFO" doc:name="Inbound logger"/>
        <set-property propertyName="tipoProtocolo" value="#[flowVars.request.tipoProtocolo]" doc:name="Property Tipo Protocolo"/>
        <choice doc:name="Web Service protocol - choice">
            <when expression="#[payload.formato=='REST']">
                <choice doc:name="REST Http method - choice">
                    <when expression="#[payload.metodo == 'GET']">
                        <logger message="#['REST HTTP GET']" level="INFO" doc:name="Get request Logger"/>
                        <choice doc:name="Choice">
                            <when expression="#[payload.paramType=='URI']">
                                <http:request config-ref="HTTP_Rest_Request_Config" path="#[payload.metodoServicio]" method="GET" doc:name="HTTP GET uri-param request">
                                    <http:request-builder>
                                        <http:uri-params expression="#[payload.get(payload.get('entidadSeleccionada'))]"/>
                                    </http:request-builder>
                                </http:request>
                            </when>
                            <when expression="#[payload.paramType=='QUERY']">
                                <http:request config-ref="HTTP_Rest_Request_Config" path="#[payload.metodoServicio]" method="GET" doc:name="HTTP GET query-param request">
                                    <http:request-builder>
                                        <http:query-params expression="#[payload.get(payload.get('entidadSeleccionada'))]"/>
                                    </http:request-builder>
                                </http:request>
                            </when>
                            <otherwise>
                                <http:request config-ref="HTTP_Rest_Request_Config" path="#[payload.metodoServicio]" method="GET" doc:name="HTTP No Params"/>
                            </otherwise>
                        </choice>
                    </when>                    
                    <otherwise>
                        <logger message="#['POST HTTP REST: ----&gt;' +payload]" level="INFO" doc:name="Post Request Logger"/>
                        <set-payload value="#[payload.get(payload.get('entidadSeleccionada'))]" doc:name="Set Payload" mimeType="application/json"/>
                        <json:object-to-json-transformer doc:name="Object to JSON"/>
                        <http:request config-ref="HTTP_Rest_Request_Config" path="#[flowVars.request.metodoServicio]" method="POST" doc:name="HTTP POST Request">
                            <http:request-builder>
                                <http:header headerName="Content-Type" value="application/json; charset=utf-8"/>
                            </http:request-builder>
                        </http:request>
                    </otherwise>
                </choice>
            </when>
            <when expression="#[payload.formato=='SOAP']">
                <logger message="#['SOAP SERVICE '+payload]" level="INFO" doc:name="SOAP web service consume"/>
                <set-payload value="#[#[payload.get(payload.get('entidadSeleccionada'))]]" mimeType="application/xml" doc:name="Set Payload"/>
                <expression-component doc:name="Expression Delete XML Version"><![CDATA[payload=payload.replace("<?xml version='1.0' encoding='windows-1252'?>","")]]></expression-component>
                <http:request config-ref="HTTP_SOAP_Request_Config" path="#[flowVars.request.metodoServicio]" method="POST" doc:name="HTTP" >                    
                    <http:request-builder>
                                <http:header headerName="soapAction" value="http://wsDoc4Us/IwsDoc4Us/ConsultarDependencias"/>
                                <http:header headerName="Content-Type" value="text/xml;charset=utf-8"/>
            		</http:request-builder>
                </http:request>
                <mulexml:dom-to-xml-transformer doc:name="DOM to XML"/>
                <json:xml-to-json-transformer doc:name="XML to JSON"/>
            </when>
            <otherwise>
                <logger message="#['Unsupported service']" level="INFO" doc:name="Unhandled exception"/>
            </otherwise>
        </choice>
        <choice doc:name="Choice Tipo Respuesta">
            <when expression="#[flowVars.request.tipoRespuesta=='ASCII']">
                <choice doc:name="Choice Formato">
                    <when expression="#[flowVars.request.formato=='SOAP']">
                        <logger level="INFO" doc:name="Logger SOAP" message="#['Cargando Mensaje Respuesta: ' + payload]"/>
                        <dw:transform-message doc:name="Transform Message">
                            <dw:set-payload><![CDATA[%dw 1.0
%output application/json
---
{
	operacion:flowVars.request.operacionDestino,
    origen:flowVars.request.aplicacionDestino,
    destino:flowVars.request.aplicacionOrigen,
    entidad:flowVars.request.entidadSeleccionada,
	carga:payload
}]]></dw:set-payload>
                        </dw:transform-message>
                    </when>
                    <otherwise>
                        <logger level="INFO" doc:name="Logger REST"/>
                        <byte-array-to-object-transformer doc:name="Byte Array to Object"/>
                        <json:object-to-json-transformer doc:name="Object to JSON"/>
                        <dw:transform-message doc:name="Transform Message">
                            <dw:set-payload><![CDATA[%dw 1.0
%output application/json
---
{
	operacion:flowVars.request.operacionDestino,
    origen:flowVars.request.aplicacionDestino,
    destino:flowVars.request.aplicacionOrigen,
    entidad:flowVars.request.entidadSeleccionada,
	carga: payload,
	correlationID:flowVars.request.correlationID
}]]></dw:set-payload>
                        </dw:transform-message>
                    </otherwise>
                </choice>
            </when>
            <otherwise>
                <logger level="INFO" doc:name="Logger "/>
            </otherwise>
        </choice>
        <object-to-string-transformer doc:name="Object to String"/>
        <logger message="#['ROUTER - HTTP REQ APP--&gt;' + payload]" level="INFO" doc:name="Logger"/>
        <http:request config-ref="HTTP_Redirect_Application_Config" path="/application" method="POST" doc:name="HTTP Redirect application">
            <http:request-builder>
                <http:header headerName="ROUTER_TYPE" value="RESPONSE"/>
            </http:request-builder>
        </http:request>
                
            </when>
            <when expression="#[message.inboundProperties.'ROUTER_TYPE'=='RESPONSE']">
                <logger message="#[&quot;JMS_RESPONSE_FINAL --&gt; &quot; + payload]" level="INFO" doc:name="Logger JMS"/>
                <set-variable variableName="responseJMS" value="#[payload]" doc:name="Variable"/>
                <set-payload value="#[payload]" mimeType="application/json" doc:name="Set Payload"/>
                <scripting:component doc:name="Groovy">
                    <scripting:script engine="Groovy"><![CDATA[message.correlationId=payload.correlationID]]></scripting:script>
                </scripting:component>
                <jms:outbound-endpoint queue="RESP_FINAL" doc:name="JMS" connector-ref="Active_MQ_RESPONSE_FINAL"/>
                
            </when>
            <otherwise>
                <logger level="INFO" doc:name="Logger" message="#['DEFAULT_RESPONSE']"/>
            </otherwise>
        </choice>
        
	</flow>
</mule>
