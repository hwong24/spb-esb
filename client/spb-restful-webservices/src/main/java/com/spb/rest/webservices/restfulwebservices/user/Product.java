package com.spb.rest.webservices.restfulwebservices.user;

public class Product {
	
	private String name;
	private Integer code;
	private String description;
		
	public Product() {}
	
	public Product(String name, int code, String description) {
		super();
		this.name = name;
		this.code = code;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Product [name=" + name + ", code=" + code + ", description=" + description + "]";
	}
}
