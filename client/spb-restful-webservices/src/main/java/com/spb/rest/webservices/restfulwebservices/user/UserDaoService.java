package com.spb.rest.webservices.restfulwebservices.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class UserDaoService {
	
	private static List<User> users = new ArrayList<>();
	private int usersCount = 5;
	
	static {
		users.add(new User(1, "Pepito Perez", new Date()));
		users.add(new User(2, "Rosa Martin", new Date()));
		users.add(new User(3, "Juan Charrasqueado", new Date()));
		users.add(new User(4, "Abraham Perez", new Date()));
		users.add(new User(5, "Pedro Pereira", new Date()));
	}
	
	public List<User> findAll(){
		return users;
	}
	
	public User save(User user) {		
		if(user.getId() == null)
			user.setId(++usersCount);
		
		users.add(user);		
		return user;
	}
	
	public User findOne(int id) {
		for(User user:users) {
			if(user.getId()==id)
				return user;
		}
		return null;
	}
	
	public User deleteById(int id) {
		Iterator<User> iterator = users.iterator();
		while(iterator.hasNext()) {
			User user = iterator.next();
			if(user.getId()==id) {
				iterator.remove();
				return user;
			}
		}
		return null;
	}

}
