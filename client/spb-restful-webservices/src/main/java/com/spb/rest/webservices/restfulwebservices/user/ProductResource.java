package com.spb.rest.webservices.restfulwebservices.user;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.spb.rest.webservices.restfulwebservices.user.exceptions.ProductNotFoundException;
import com.spb.rest.webservices.restfulwebservices.user.exceptions.UserNotFoundException;

@RestController
public class ProductResource {
	@Autowired
	private ProductDaoService service;

	@GetMapping("/products")
	public List<Product> retrieveAllUsers() {
		return service.findAll();
	}

	@GetMapping("/products/{id}")
	public Product retrieveUser(@PathVariable int id) {
		Product product = service.findOne(id);
		if(product==null)
			throw new ProductNotFoundException("id-"+id);
		return product;
	}

	@PostMapping("/products")
	public ResponseEntity<Object> createuser(@RequestBody Product product) {
		Product savedProduct = service.save(product);
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(savedProduct)
				.toUri();
		return ResponseEntity.created(location).build();
	}
	
	@DeleteMapping("/products/{id}")
	public void deleteUser(@PathVariable int id) {
		Product product = service.deleteById(id);
		if(product==null)
			throw new UserNotFoundException("id-"+id);
	}
}
