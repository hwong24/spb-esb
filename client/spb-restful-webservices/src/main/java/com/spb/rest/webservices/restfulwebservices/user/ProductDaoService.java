package com.spb.rest.webservices.restfulwebservices.user;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class ProductDaoService {
	private static List<Product> products = new ArrayList<>();
	private int productsCount = 4;

	static {
		products.add(new Product("CART", 7, "Cartera"));
		products.add(new Product("CRED", 21, "Credito"));
		products.add(new Product("CES", 4, "Cesantias"));
		products.add(new Product("AVOL", 3, "Ahorro Voluntario"));
	}

	public List<Product> findAll() {
		return products;
	}

	public Product save(Product product) {
		if (product.getCode() == null)
			product.setCode(++productsCount);

		products.add(product);
		return product;
	}

	public Product findOne(int id) {
		for (Product p : products) {
			if (p.getCode() == id)
				return p;
		}
		return null;
	}

	public Product deleteById(int id) {
		Iterator<Product> iterator = products.iterator();
		while (iterator.hasNext()) {
			Product p = iterator.next();
			if (p.getCode() == id) {
				iterator.remove();
				return p;
			}
		}
		return null;
	}
}
