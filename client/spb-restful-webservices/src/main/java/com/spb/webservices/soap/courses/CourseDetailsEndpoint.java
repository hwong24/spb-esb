package com.spb.webservices.soap.courses;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.server.endpoint.annotation.SoapAction;

import com.spb.courses.CourseDetails;
import com.spb.courses.GetCourseDetailsRequest;
import com.spb.courses.GetCourseDetailsResponse;

@Endpoint
public class CourseDetailsEndpoint {
	@PayloadRoot(namespace="http://spb.com/courses",localPart="GetCourseDetailsRequest")
	@ResponsePayload
	//@SoapAction(value = "http://spb.com/courses/GetCourseDetailsRequest")
	public GetCourseDetailsResponse processCourseDetailRequest
	(@RequestPayload GetCourseDetailsRequest request) {
		GetCourseDetailsResponse response = new GetCourseDetailsResponse();
		
		CourseDetails courseDetails = new CourseDetails();
		courseDetails.setId(request.getId());
		courseDetails.setName("Apache Spark Course 3");
		courseDetails.setDescription("You would learn the basics of Apache Spark 3");
		response.setCourseDetails(courseDetails );
		return response;
	}

}
